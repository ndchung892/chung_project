//
//  StoryBoardManager.swift
//  CustomViewSample
//
//  Created by Minh Đại on 4/7/20.
//  Copyright © 2020 GST.SYN. All rights reserved.
//

import UIKit

class StoryBoardManager: NSObject {
    // MARK: - Storyboard
    static let mainStoryBoardName: String = "Main"

    
    // MARK: - Instance ViewController in Main Storyboard
    static func instanceAddViewController() -> AddViewController {
        let storyBoard = UIStoryboard(name: mainStoryBoardName, bundle: nil)
        let addView = storyBoard.instantiateViewController(withIdentifier: "AddViewController") as! AddViewController
        
        return addView
    }
}
