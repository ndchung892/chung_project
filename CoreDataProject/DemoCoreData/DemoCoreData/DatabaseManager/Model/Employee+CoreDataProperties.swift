//
//  Employee+CoreDataProperties.swift
//  DemoCoreData
//
//  Created by Chung on 08/02/2023.
//  Copyright © 2023 GST.SYN. All rights reserved.
//
//

import Foundation
import CoreData


extension Employee {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Employee> {
        return NSFetchRequest<Employee>(entityName: "EmployeeEntity")
    }

    @NSManaged public var employeeId: Int64
    @NSManaged public var name: String?
    @NSManaged public var age: Int16
    @NSManaged public var department: Department?

}
