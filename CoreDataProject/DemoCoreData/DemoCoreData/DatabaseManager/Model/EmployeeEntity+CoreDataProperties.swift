//
//  EmployeeEntity+CoreDataProperties.swift
//  DemoCoreData
//
//  Created by Chung on 08/02/2023.
//  Copyright © 2023 GST.SYN. All rights reserved.
//
//

import Foundation
import CoreData


extension EmployeeEntity {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<EmployeeEntity> {
        return NSFetchRequest<EmployeeEntity>(entityName: "EmployeeEntity")
    }

    @NSManaged public var employeeId: String?
    @NSManaged public var name: String?
    @NSManaged public var age: Int16
    @NSManaged public var department: DepartmentEntity?

}
