//
//  DepartmentEntity+CoreDataProperties.swift
//  DemoCoreData
//
//  Created by Chung on 08/02/2023.
//  Copyright © 2023 GST.SYN. All rights reserved.
//
//

import Foundation
import CoreData


extension DepartmentEntity {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<DepartmentEntity> {
        return NSFetchRequest<DepartmentEntity>(entityName: "DepartmentEntity")
    }

    @NSManaged public var departmentId: String?
    @NSManaged public var name: String?
    @NSManaged public var employee: NSSet?

}

// MARK: Generated accessors for employee
extension DepartmentEntity {

    @objc(addEmployeeObject:)
    @NSManaged public func addToEmployee(_ value: EmployeeEntity)

    @objc(removeEmployeeObject:)
    @NSManaged public func removeFromEmployee(_ value: EmployeeEntity)

    @objc(addEmployee:)
    @NSManaged public func addToEmployee(_ values: NSSet)

    @objc(removeEmployee:)
    @NSManaged public func removeFromEmployee(_ values: NSSet)

}
