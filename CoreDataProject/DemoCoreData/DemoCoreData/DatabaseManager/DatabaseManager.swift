//
//  DatabaseManager.swift
//  DemoCoreData
//
//  Created by Chung on 08/02/2023.
//  Copyright © 2023 GST.SYN. All rights reserved.
//

import Foundation
import CoreData

class DatabaseManager: NSObject {
    static let shared: DatabaseManager = DatabaseManager()
        
    // MARK: - Core Data stack
    
    private let modelName: String = "Company"
    
    private let entityDepartmentName: String = "DepartmentEntity"
    private let entityEmployeeName: String = "EmployeeEntity"
    private let entityPersonName: String = "PersonEntity"
    
    // MARK: - Core Data Stack
    // Step 1: NSManagedObjectModel
    private lazy var managedObjectModel: NSManagedObjectModel? = {
        guard let modelURL = Bundle.main.url(forResource: modelName, withExtension: "momd") else {
            print("modelURL error")
            return nil
        }
        
        guard let managedObjectModel = NSManagedObjectModel(contentsOf: modelURL) else {
            print("managedObjectModel contentsOfUrl error")
            return nil
        }
        
        return managedObjectModel
    }()
    
    // Step 2: NSPersistentStoreCoordinator
    private lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator? = {
        guard let managedObjectModel = managedObjectModel else {
            return nil
        }
        let persistentStoreCoordinator = NSPersistentStoreCoordinator(managedObjectModel: managedObjectModel)
        
        let fileManager = FileManager.default
        let storeName = "\(modelName).sqlite"
        
        let documentsDirectoryURL = fileManager.urls(for: .documentDirectory, in: .userDomainMask)[0]
        
        let persistentStoreURL = documentsDirectoryURL.appendingPathComponent(storeName)
        
        do {
            let options = [NSMigratePersistentStoresAutomaticallyOption: true, NSInferMappingModelAutomaticallyOption: true]
            try persistentStoreCoordinator.addPersistentStore(ofType: NSSQLiteStoreType,
                                                              configurationName: nil,
                                                              at: persistentStoreURL,
                                                              options: options)
        } catch let error as NSError {
            print("persistentStoreCoordinator error \(error), \(error.userInfo)")
            return nil
        }
        
        return persistentStoreCoordinator
    }()

    // Step 3: NSManagedObjectContext
    private lazy var managedObjectContext: NSManagedObjectContext = {
        let managedObjectContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        
        managedObjectContext.persistentStoreCoordinator = persistentStoreCoordinator
        
        return managedObjectContext
    }()
    
    
    // MARK: - Private Common Function
    private func saveContext() -> Bool {
        if !managedObjectContext.hasChanges {
            return true
        }
        
        do {
            try managedObjectContext.save()
            return true
        } catch let error as NSError {
            print("Unresolved error \(error), \(error.userInfo)")
            managedObjectContext.rollback()
            return false
        }
    }
    
    // Get - Set
    private func getEntity(entityName: String, sortPredicate: NSPredicate? = nil) -> [Any]? {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
        fetchRequest.returnsObjectsAsFaults = false
        fetchRequest.predicate = sortPredicate
        
        do {
            let fetchObjects = try managedObjectContext.fetch(fetchRequest)
            return fetchObjects
        } catch let error as NSError {
            print("Fetch error \(error), \(error.userInfo)")
            return nil
        }
    }
    
    private func getAllEntities(entityName: String, predicate: NSPredicate?, sortDescriptors: [NSSortDescriptor]) -> [Any]? {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
        fetchRequest.predicate = predicate
        fetchRequest.sortDescriptors = sortDescriptors
        
        do {
            let fetchObjects = try managedObjectContext.fetch(fetchRequest)
            return fetchObjects
        } catch let error as NSError {
            print("Fetch error \(error), \(error.userInfo)")
            return nil
        }
    }
    
    private func removeItemWithEntity(entityName: String, predicate: NSPredicate) -> Bool {
        let fetchObjects = getEntity(entityName: entityName, sortPredicate: predicate)
        guard let objects = fetchObjects else {
            return false
        }
        
        for object in objects {
            managedObjectContext.delete(object as! NSManagedObject)
        }
        return saveContext()
    }
    
    private func removeAllWithEntity(entityName: String) -> Bool {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
        let request = NSBatchDeleteRequest(fetchRequest: fetchRequest)
        
        do {
            try managedObjectContext.execute(request)
            return saveContext()
        } catch let error as NSError {
            print("Fetch error \(error), \(error.userInfo)")
            return false
        }
    }
}

extension DatabaseManager {
    // insert
    // MARK: - DepartmentEntity function
    private func newDepartment() -> DepartmentEntity {
        let entity = NSEntityDescription.insertNewObject(forEntityName: entityDepartmentName,
                                                         into: managedObjectContext)
        return entity as! DepartmentEntity
    }
        
    func addNewDepartment(name: String?, departmendId: String?) -> Bool {

        let entity = newDepartment()
        entity.departmentId = departmendId
        entity.name = name
        return saveContext()
    }
    
    func getAllDepartment() -> [DepartmentEntity]? {
        return getAllEntities(entityName: entityDepartmentName,
                              predicate: nil,
                              sortDescriptors: []) as? [DepartmentEntity]
    }
    
    
        
    // insert
    // MARK: - DeliverInfo function
    private func newEmployee() -> EmployeeEntity {
        let entity = NSEntityDescription.insertNewObject(forEntityName: entityEmployeeName,
                                                         into: managedObjectContext)
        return entity as! EmployeeEntity
    }
        
    func addNewEmployee(name: String?, id: String, age: Int?, department: DepartmentEntity) -> Bool {
        let entity = newEmployee()
        entity.employeeId = id
        entity.name = name
        entity.age = Int16(age ?? 0)
        entity.department = department
        return saveContext()
    }
    
    func updateEmployee(employeeId: String, name: String?, age: Int?, department: DepartmentEntity) -> Bool {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entityEmployeeName)
        fetchRequest.predicate = NSPredicate(format: "employeeId == %@", employeeId)
        do {
            let results = try managedObjectContext.fetch(fetchRequest)
            if let entity = results.first as? EmployeeEntity {
                entity.name = name
                entity.age = Int16(age ?? 0)
                entity.department = department
            }
        } catch {
            print(error.localizedDescription)
        }

        return saveContext()
    }

    func getAllEmployee() -> [EmployeeEntity]? {
        return getAllEntities(entityName: entityEmployeeName,
                              predicate: nil,
                              sortDescriptors: []) as? [EmployeeEntity]
    }
    
    // Sort
    func getAllEmployeeByAge() -> [EmployeeEntity]? {
        let sortDescriptor = NSSortDescriptor(key: "age", ascending: true)
        let sortDescriptors = [sortDescriptor]

        return getAllEntities(entityName: entityEmployeeName,
                              predicate: nil,
                              sortDescriptors: sortDescriptors) as? [EmployeeEntity]
    }
    
    // Delete
    
    func deleteEmployee(employeeId: String) {
        let predicate = NSPredicate(format: "employeeId == %@", employeeId)
        _ = removeItemWithEntity(entityName: entityEmployeeName, predicate: predicate)
    }

}
