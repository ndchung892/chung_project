//
//  EmployeeInfoCell.swift
//  DemoCoreData
//
//  Created by Minh Đại on 4/27/20.
//  Copyright © 2020 GST.SYN. All rights reserved.
//

import UIKit

class EmployeeInfoCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var ageLabel: UILabel!
    @IBOutlet weak var departmentLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setupData(data: EmployeeEntity) {
        nameLabel.text = data.name
        ageLabel.text = "\(data.age)"
        departmentLabel.text = data.department?.name
    }
}
