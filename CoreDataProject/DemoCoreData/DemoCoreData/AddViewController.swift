//
//  AddViewController.swift
//  DemoCoreData
//
//  Created by Minh Đại on 4/26/20.
//  Copyright © 2020 GST.SYN. All rights reserved.
//

import UIKit

enum ScreenType {
    case add
    case edit
}

class AddViewController: UIViewController {

    @IBOutlet weak var employeeIdTextField: UITextField!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var ageTextField: UITextField!
    @IBOutlet weak var departmentButton: UIButton!
    
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var departmentPickerView: UIPickerView!
   
    var dataPicker: [DepartmentEntity] = []
    var screenType: ScreenType = .add
    var currentEmployee: EmployeeEntity?
    var currentDepartment: DepartmentEntity?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Dismiss keyboard
        let tap = UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing(_:)))
        view.addGestureRecognizer(tap)

        title = screenType == .add ? "Add employee" : "Edit employee"
        deleteButton.isHidden = screenType == .add
      
        dataPicker = DatabaseManager.shared.getAllDepartment() ?? []
        departmentButton.setTitle(dataPicker.first?.name, for: .normal)
        currentDepartment = dataPicker.first
        departmentPickerView.isHidden = true
        setupEmployeeData()
        
        
    }
    
    @IBAction func didTapChooseDepartmentButton(_ sender: Any) {
        departmentPickerView.isHidden = false
        departmentPickerView.dataSource = self
        departmentPickerView.delegate = self
        view.endEditing(true)
    }
    
    func setupEmployeeData() {
        guard let employee = currentEmployee else {
            return
        }

        employeeIdTextField.text = employee.employeeId
        nameTextField.text = employee.name
        ageTextField.text = "\(employee.age)"
        currentDepartment = employee.department
        departmentButton.setTitle(employee.department?.name, for: .normal)
    }
    
    @IBAction func didTapDeleteButton(_ sender: Any) {
        guard let id = employeeIdTextField.text else {
            showDialog(message: "EmployeeId or department is require")
            return
        }
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func didTapSaveButton(_ sender: Any) {
        guard let id = employeeIdTextField.text, let department = currentDepartment else {
            showDialog(message: "EmployeeId or department is require")
            return
        }
        guard let nameStr = nameTextField.text else{
            showDialog(message: "name is require")
            return
        }
        guard let ageStr = ageTextField.text else{
            showDialog(message: "age is require")
            return
        }
        let ageInt = Int(ageStr)

        if screenType == .edit {
            navigationController?.popViewController(animated: true)
            return
        }

        navigationController?.popViewController(animated: true)
        _ = DatabaseManager.shared.addNewEmployee(name: nameStr, id: id, age: ageInt, department: department)
    }
    
    func showDialog(message: String) {
        let alertViewController: UIAlertController = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        let action: UIAlertAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) { (_) in
        }
        alertViewController.addAction(action)
        alertViewController.modalPresentationStyle = .overFullScreen
        present(alertViewController, animated: true, completion: nil)
    }
}

extension AddViewController: UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
        
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return dataPicker.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let name: String = dataPicker[row].name ?? ""
        
        return name
    }
    
    
}

extension AddViewController: UIPickerViewDelegate {
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let name: String = dataPicker[row].name ?? ""
        departmentButton.setTitle(name, for: .normal)
        currentDepartment = dataPicker[row]
    }
}
