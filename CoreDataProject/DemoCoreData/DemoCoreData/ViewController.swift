//
//  ViewController.swift
//  DemoCoreData
//
//  Created by Minh Đại on 4/25/20.
//  Copyright © 2020 GST.SYN. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var isSortByAge: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        title = "Employee"
        addDefaultDepartment()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.register(UINib(nibName: "EmployeeInfoCell", bundle: nil), forCellReuseIdentifier: "EmployeeInfoCell")
        
        

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        tableView.reloadData()
    }

    
    @IBAction func didTapButtonAdd(_ sender: Any) {
        let addViewController = StoryBoardManager.instanceAddViewController()
        
        navigationController?.pushViewController(addViewController, animated: true)
    }
    
    func addDefaultDepartment() {
        let dbManager = DatabaseManager.shared
        guard let currentDepartments = dbManager.getAllDepartment() else {
            return
        }
        
        if currentDepartments.isEmpty {
            _ = [
                dbManager.addNewDepartment(name: "Department", departmendId: "depart"),
                dbManager.addNewDepartment(name: "Department1", departmendId: "depart1"),
                dbManager.addNewDepartment(name: "Department2", departmendId: "depart2"),
                dbManager.addNewDepartment(name: "Department3", departmendId: "depart3")
            ]
        }
    }
    
    
    func getAllEmployee() -> [EmployeeEntity] {
        return DatabaseManager.shared.getAllEmployee() ?? []
        
    }

    @IBAction func sortByAge(_ sender: Any) {
        isSortByAge = true
        tableView.reloadData()
    }
}


// MARK: - UITableViewDataSource
extension ViewController: UITableViewDataSource {
        
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return getAllEmployee().count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "EmployeeInfoCell", for: indexPath) as! EmployeeInfoCell
        let data = getAllEmployee()[indexPath.row]
        cell.setupData(data: data)
        return cell
    }
}

// MARK: - UITableViewDelegate
extension ViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let editView = StoryBoardManager.instanceAddViewController()
        editView.screenType = .edit
        editView.currentEmployee = getAllEmployee()[indexPath.row]
        navigationController?.pushViewController(editView, animated: true)
    }
}
